*** Settings ***
Library    SeleniumLibrary        timeout=10        run_on_failure=Capture Page Screenshot
Library    String
Library    BuiltIn
Library    OperatingSystem

*** Variables ***
${BROWSER}                  chrome
${URL}                  https://yourpay.co/
# ${SECRET_KEY_LOGIN}         qvcy6f7c64uniu5w3lsu5dyziyjukvyp

*** Keywords ***
Begin Web Test
    # Remove Files                    /Users/muhammad.anggadirja/Documents/te_backyard_sso_automation_web/report/*.png
    Open Browser                    about:blank            ${BROWSER}
    Maximize Browser Window
    Go To                           ${URL}

End Web Test
    Close Browser