*** Settings ***
Resource        ../config/web.robot
Resource        ../testScenario/home.robot
Suite Setup     Begin Web Test
Suite Teardown  End Web Test


*** Test Cases ***
Test Home
    [Documentation]            This scenario test User can access URL Home Page
    [Tags]                     homePage    home    test
    user can access URL Home Page

Test Product Page
    [Documentation]            This scenario test User can access URL Product Page
    [Tags]                     productPage    product    test
    user can go to Product Page

Test Partners Page
    [Documentation]            This scenario test User can access URL Partners Page
    [Tags]                     partnesPage    partners    test
    user can go to Partners Page

Text Newsroom Page
    [Documentation]            This scenario test User can access URL Newsroom Page
    [Tags]                     newsroomPage    newsroom    test
    user can go to Newsroom Page

Text Blog Page
    [Documentation]            This scenario test User can access URL Blog Page
    [Tags]                     blogPage    blog    test
    user can go to Blog Page


# Run
# robot -d report/ -v env:web -i test testSuite/test_home.robot  