*** Settings ***
Library        SeleniumLibrary

*** Variables ***
# First Content
${home_first_content}            xpath=//div[@class='left']/p[@class='desktop']
${slogan_content}                xpath=//div[@class='left']//p[@class='slogan']
${maps}                          xpath=//div[@class='maps']
${phone}                         xpath=//div[@class='phone']
${buttonGooglePlay}              xpath=//div[@class='left']/div[@class='btn-new-google-play']
${home_second_content}           xpath=//div[@class='home-second-content']//div[@class='row wrapper-content']/div

# Meet YourPay
${home_meetYourPage}             xpath=//div[@class='home-meet-yourpay']
${title_meetYourPage}            xpath=//div[@class='home-meet-yourpay']/h2
${content_meetYourPage}          xpath=//div[@class='home-meet-yourpay']/p
${img_meetYourPage}              xpath=//div[@class='home-meet-yourpay']/img

# Why Choose Yourpay
${homeThird_mainTitle}           xpath=//div[@class='home-third-content']//p[@class='main-title']
${home_third_content}            xpath=//div[@class='home-third-content']//div[@class='row wrapper-content']/div

# Main Feature
${home_fourth_content}           xpath=//div[@class='home-fourth-content']//div[@class='container']/p
${homeFourth_textContent}        xpath=//div[@class='home-fourth-content']//div[@class='container']/div


# Latest Article
${articleSection}                xpath=//div[@class='container']/div[@class='section-article-home']
${textArticleHeader}             xpath=//div[@class='container']/div[@class='section-article-home']/h3
${contentArticle}                xpath=//div[@class='container']/div[@class='section-article-home']/div[@class='row']/div

*** Keywords ***
user is on the Hemo Page
    Wait Until Element Is Visible    ${home_first_content}
    ${first_content}    Get Text     ${home_first_content}
    ${slogan}           Get Text     ${slogan_content}
    Element Should Be Visible        ${maps}
    Element Should Be Visible        ${phone}
    Element Should Be Visible        ${buttonGooglePlay}
    ${length}    Get Length          ${home_second_content}

        # Ini Untuk Mengambil Text dari element home-second-content 'wrapper-overflow'
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${home_second_content}\[${counter}]
            IF    ${result}
                ${title}             Get Text    //div[@class='home-second-content']//div[@class='row wrapper-content']/div[${counter}]//p[@class='title']
                ${subTitle}          Get Text    //div[@class='home-second-content']//div[@class='row wrapper-content']/div[${counter}]//p[@class='subtitle']
                Log To Console    ${title}${SPACE}${subTitle}
            ELSE
                BREAK
            END
        END
    
    Log To Console    ${\n}${first_content}
    Log To Console    ${slogan}

user scroll to Meet YourPay
    Wait Until Element Is Visible     ${home_meetYourPage}
    ${textMeetYourPay}    Get Text    ${title_meetYourPage}
    Element Should Be Visible         ${content_meetYourPage}
    Element Should Be Visible         ${img_meetYourPage}
    Log To Console    ${textMeetYourPay}

user scroll to Why Choose Yourpay
    Wait Until Element Is Visible    ${homeThird_mainTitle}
    ${maintitle}    Get Text         ${homeThird_mainTitle}
    Scroll Element Into View         ${home_third_content}
    ${length}    Get Length          ${home_third_content}
    
    # Ini Untuk Mengambil Text dari element home-third-content 'wrapper-overflow'
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${home_third_content}\[${counter}]
            IF    ${result}
                ${title}             Get Text    //div[@class='home-third-content']//div[@class='row wrapper-content']/div[${counter}]//p[@class='title']
                ${subTitle}          Get Text    //div[@class='home-third-content']//div[@class='row wrapper-content']/div[${counter}]//p[@class='subtitle']
                Log To Console    - ${title}${SPACE}${subTitle}${\n}
            ELSE
                BREAK
            END
        END

    Log To Console    Main Title = ${mainTitle}


user scroll to Main Features
    Wait Until Element Is Visible        ${home_fourth_content}
    Scroll Element Into View             ${home_fourth_content}
    ${length}    Get Length              ${homeFourth_textContent}
    
    # Ini Untuk Mengambil Text dari element home-fourth-content 'wrapper-overflow'
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${homeFourth_textContent}\[${counter}]//p
            IF    ${result}
                ${title}             Get Text    //div[@class='home-fourth-content']//div[@class='container']/div[${counter}]//p
                Log To Console   Main Feature :- ${title}${SPACE}${\n}
            ELSE
                BREAK
            END
        END

user scroll to Yourpay Latest Articles
    Wait Until Element Is Visible        ${articleSection}
    Scroll Element Into View             ${articleSection}

    ${textArticle}    Get Text           ${textArticleHeader}
    
    ${length}    Get Length              ${contentArticle}
    # Ini Untuk Mengambil Text dari element home-fourth-content 'wrapper-overflow'
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${contentArticle}\[${counter}]
            IF    ${result}
                ${title}             Get Text    //div[@class='container']/div[@class='section-article-home']/div[@class='row']/div[${counter}]//div[@class='card-body content-area']/p[@class='card-text title']
                Log To Console   Latest Article :- ${title}${SPACE}${\n}
            ELSE
                BREAK
            END
        END