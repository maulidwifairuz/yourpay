*** Settings ***
Library            SeleniumLibrary

*** Variables ***
${clickNavBarBlog}                    xpath=//div//a[contains(text(), 'Blog')]
${headerMostPopular}                  xpath=//div[@class='content-right']/h3
${listBlogPopular}                    xpath=//div[@class='content-right']/ul/li
${yourPayArticle}                     xpath=//div[@class='container']/p
${listArticle}                        xpath=//div[@class='container']/div[@class='row']/div[@class='col-lg-4 card-group']

*** Keywords ***
user go to Blog Page
    Sleep    3
    Click Element                     ${clickNavBarBlog}
    Element Should Be Visible         ${headerMostPopular}

verify Most Popular Blog
    Element Should Be Visible         ${headerMostPopular}

    ${length}    Get Length           ${listBlogPopular}

    # Ini Untuk Mengambil Text dari element Most Popular Blog
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${listBlogPopular}\[${counter}]
            IF    ${result}
                ${title}             Get Text    //div[@class='content-right']/ul/li[${counter}]/a/div/h6
                Log To Console    ${title}${SPACE}${\n}
            ELSE
                BREAK
            END
        END

verify YourPay Article
    Sleep    1
    Element Should Be Visible         ${yourPayArticle}
    ${length}    Get Length           ${listArticle}
    
    # Ini Untuk Mengambil Text dari element Yourpay Article
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${listArticle}\[${counter}]
            IF    ${result}
                ${title}             Get Text    //div[@class='container']/div[@class='row']/div[@class='col-lg-4 card-group'][${counter}]//a/div/p[@class='card-text title']
                Log To Console    ${title}${SPACE}${\n}
            ELSE
                BREAK
            END
        END