*** Settings ***
Library        SeleniumLibrary
Resource       ../../PageObject/Home/homePage.robot

*** Variables ***
${clickNavBarProduct}               xpath=//div//a[contains(text(), 'Product')]
${productPageTitle}                 xpath=//div//p[contains(text(), 'Yourpay To The Rescue!')]
${getTitle}                         xpath=//div//h2
${buttonDownload}                   xpath=//div[@class='content-right']/img
${fifth_content}                    xpath=//div[@class='fifth-content']/p
${vidio_yt}                         xpath=//div[@class='fifth-content']/div

# Content License
${licenseContent}                   xpath=//div[@class='container']/p
${licenseBI}                        xpath=//div[@class='container']/div[@class='wrapper-lisensi-bank-indonesia']
${licenseKominfo}                   xpath=//div[@class='container']/div[@class='wrapper-lisensi-kominfo']

# Content Product
${thirdTitleProduct}                xpath=//div[@class='container']/div[@class='third-content-product']/h1
${tab_topUp}                        xpath=//div[@class='col-lg-6 align-self-center']/h1
${buttonTutorial_tab}               xpath=//div[@class='col-lg-6 align-self-center']/button

# Nav Tab Send Money
${tab_SendMoney}                    xpath=//*[contains(text(), 'Send Money')]
${headerTab_sendMoney}              xpath=//div[@class='col-lg-6 align-self-center']/h1

# Nav tab Bill & Purchase
${tab_Bill}                         xpath=//*[contains(text(), 'Bill Purchase & payment')]
${headerTab_nav}                    xpath=//div[@class='col-lg-6 align-self-center']/h1

# Nav Tab Cash Withdraw
${tab_CashWithdraw}                 xpath=//*[contains(text(), 'Cash Withdraw')]
${buttonPartner_tabCashWithdraw}    xpath=//div[@class='col-lg-6 align-self-center']/button

# Nav Tab Pay Shopping
${tab_pay}                          xpath=//*[contains(text(), 'Pay Shopping')]

# Nav Tab Charity Payment
${tab_charity}                      xpath=//div[@class='third-content-product']/ul/li/a[contains(text(), 'Charity Payment')]
*** Keywords ***
user go to Product Page
    user is on the Hemo Page
    Click Element                    ${clickNavBarProduct}
    Wait Until Element Is Visible    ${productPageTitle}
    ${titlePageProduct}    Get Text  ${getTitle}
    Element Should Be Visible        ${buttonDownload}
    ${currentURL}      Get Location

    Log To Console    Title Product : ${titlePageProduct}${\n}
    Log To Console    Path Current : ${currentURL}

verify Content License
    Wait Until Element Is Visible    ${licenseContent}
    Element Should Be Visible        ${licenseBI}
    Element Should Be Visible        ${licenseKominfo}

verify Content Product
    Wait Until Element Is Visible    ${thirdTitleProduct}
    ${titleHeader_contentProduct}    Get Text    ${thirdTitleProduct}
    ${text_TopUp}                    Get Text    ${tab_topUp}

    # Default Tab Top Up
    Element Should Be Visible        ${tab_topUp}
    Element Should Be Visible        ${buttonTutorial_tab}
    
    
    Log To Console    Header Content Product : ${titleHeader_contentProduct}${\n}
    Log To Console    Top Up Tab : ${text_TopUp}${\n}

verify Tab Send Money
    Element Should Be Visible        ${tab_topUp}
    Scroll Element Into View         ${tab_SendMoney}
    Click Element                    ${tab_SendMoney}
    Element Should Be Visible        ${headerTab_sendMoney}
    Element Should Be Visible        ${buttonTutorial_tab}
    
    ${textHeader_sendMoney}          Get Text    ${headerTab_sendMoney}

    Log To Console    Header Tab Send Money : ${textHeader_sendMoney}

verify Tab Bill & Purchase
    Element Should Be Visible        ${tab_topUp}
    Scroll Element Into View         ${tab_Bill}
    Click Element                    ${tab_Bill}
    Element Should Be Visible        ${headerTab_nav}
    Element Should Be Visible        ${buttonTutorial_tab}

    ${textHeader_Bill}               Get Text    ${headerTab_nav}

    Log To Console    Header Tab Bill : ${textHeader_Bill}

verify Tab Cash Withdraw
    Element Should Be Visible        ${tab_topUp}
    Scroll Element Into View         ${tab_CashWithdraw}
    Click Element                    ${tab_CashWithdraw}
    Element Should Be Visible        ${headerTab_nav}
    Element Should Be Visible        ${buttonPartner_tabCashWithdraw}

    ${textHeader_CashWithdraw}        Get Text    ${headerTab_nav}
    Log To Console    Header Tab Cash Withdraw : ${textHeader_CashWithdraw}

verify Tab Pay Shopping
    Element Should Be Visible        ${tab_topUp}
    Scroll Element Into View         ${tab_pay}
    Click Element                    ${tab_pay}
    Element Should Be Visible        ${headerTab_nav}
    Element Should Be Visible        ${buttonTutorial_tab}

    ${textHeader_payShopping}        Get Text    ${headerTab_nav}
    Log To Console    Header Tab Cash Withdraw : ${textHeader_payShopping}

verify Tab Charity Payment
    Element Should Be Visible        ${tab_topUp}
    Scroll Element Into View         ${headerTab_nav}
    Click Element                    ${tab_charity}
    Element Should Be Visible        ${headerTab_nav}

    ${textHeader_Charity}        Get Text    ${headerTab_nav}
    Log To Console    Header Tab Cash Withdraw : ${textHeader_Charity}

verify Vidio Tutorial in Product Page
    Element Should Be Visible        ${fifth_content}
    Scroll Element Into View         ${fifth_content}
    Element Should Be Visible        ${vidio_yt}
    