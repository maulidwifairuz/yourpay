*** Settings ***
Library            SeleniumLibrary

*** Variables ***
${clickNavBarNewsroom}                        xpath=//div//a[contains(text(), 'Newsroom')]
${titleNewsroom}                              xpath=//p[@class='title-content-top']
${latestNews}                                 xpath=//div[@class='container']/div[@class='row']/div

*** Keywords ***
user go to Newsroom Page
    Sleep    3
    Click Element                             ${clickNavBarNewsroom}
    Sleep    3
    Element Should Be Visible                 ${titleNewsroom}

verify Latest News
    Wait Until Element Is Visible             xpath=//div[@class='container']/h1
    Sleep    2
    Scroll Element Into View                  ${latestNews}
    ${length}    Get Length                   ${latestNews}

    # Ini Untuk Mengambil Text dari element second-content-partner 'title-content-layanan'
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${latestNews}\[${counter}]
            IF    ${result}
                ${title}             Get Text    //div[@class='container']/div[@class='row']/div[${counter}]//div/p[@class='card-text title']
                Log To Console    ${title}${SPACE}${\n}
            ELSE
                BREAK
            END
        END