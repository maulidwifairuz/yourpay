*** Settings ***
Library            SeleniumLibrary

*** Variables ***
${clickNavBarPartners}                        xpath=//div//a[contains(text(), 'Partners')]
${headerPartner}                              xpath=//div[@class='content-right']/h2
${partnerService}                             xpath=//div[@class='second-content-partner']/p
${partnerCard}                                xpath=//div[@class='row scrollable-content-partner']/div
${YourpayPartner}                             xpath=//div[@class='third-content-partner']/p
${partnerImg}                                 xpath=//div[@class='third-content-partner']/div[@class='row']/div

*** Keywords ***
user go to Partner Page
    Sleep    3
    Click Element                                 ${clickNavBarPartners}
    Wait Until Element Is Visible                 ${headerPartner}

    ${textHeader_partners}            Get Text    ${headerPartner}

    Log To Console      Header Text Partner :     ${textHeader_partners}

verify Partner Service
    Sleep    3
    Wait Until Element Is Visible             ${headerPartner}
    Element Should Be Visible                 ${partnerService}
    
verify card partner service
    verify Partner Service
    Scroll Element Into View                  ${partnerService}

    ${length}    Get Length                   ${partnerCard}

    # Ini Untuk Mengambil Text dari element second-content-partner 'title-content-layanan'
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${partnerCard}\[${counter}]
            IF    ${result}
                ${title}             Get Text    //div[@class='row scrollable-content-partner']/div[${counter}]/div/p
                Log To Console    ${title}${SPACE}${\n}
            ELSE
                BREAK
            END
        END

verify Yourpay Partners
    Wait Until Element Is Visible                ${YourpayPartner}

    ${length}    Get Length                   ${partnerImg}

    # Ini Untuk Mengambil Text dari element second-content-partner 'title-content-layanan'
        FOR    ${counter}    IN RANGE    1    ${length}
            ${result}    Run Keyword And Return Status    Element Should Be Visible    ${partnerImg}\[${counter}]
            IF    ${result}
                ${title}             SeleniumLibrary.Get Element Attribute    //div[@class='third-content-partner']/div[@class='row']/div[${counter}]/div/img    src
                Log To Console  Image Partner YourPay:   ${title}${SPACE}${\n}
            ELSE
                BREAK
            END
        END