*** Settings ***
Resource        ../PageObject/Home/homePage.robot
Resource        ../PageObject/product/productPage.robot
Resource        ../PageObject/partners/partnersPage.robot
Resource        ../PageObject/newsroom/newsroomPage.robot
Resource        ../PageObject/blog/blogPage.robot

*** Keywords ***
user can access URL Home Page
    Given user is on the Hemo Page
    When user scroll to Meet YourPay
    Then user scroll to Why Choose Yourpay
    And user scroll to Main Features
    And user scroll to Yourpay Latest Articles

user can go to Product Page
    Given user go to Product Page
    When verify Content License
    Then verify Content Product
    And verify Tab Send Money
    And verify Tab Bill & Purchase
    And verify Tab Cash Withdraw
    And verify Tab Pay Shopping
    And verify Tab Charity Payment
    And verify Vidio Tutorial in Product Page

user can go to Partners Page
    Given user go to Partner Page
    When verify Partner Service
    Then verify card partner service
    And verify Yourpay Partners

user can go to Newsroom Page
    Given user go to Newsroom Page
    When verify Latest News

user can go to Blog Page
    Given user go to Blog Page
    When verify Most Popular Blog
    Then verify YourPay Article